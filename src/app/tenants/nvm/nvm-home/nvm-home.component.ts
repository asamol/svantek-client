import { Component, OnInit } from '@angular/core';
import { PrintClickService } from '../../../shared/services/print-click.service';


@Component({
  selector: 'app-nvm-home',
  templateUrl: './nvm-home.component.html',
  styleUrls: ['./nvm-home.component.scss'],
  providers: [PrintClickService]
})
export class NvmHomeComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
