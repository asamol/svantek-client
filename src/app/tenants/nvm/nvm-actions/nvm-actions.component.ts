import { Component, OnInit } from '@angular/core';
import { PrintClickService } from '../../../shared/services/print-click.service';


@Component({
  selector: 'app-nvm-actions',
  templateUrl: './nvm-actions.component.html',
  styleUrls: ['./nvm-actions.component.scss'],
})
export class NvmActionsComponent {

  constructor(private printClickService: PrintClickService) { }

  print(){
    this.printClickService.printClick();
  }
}
