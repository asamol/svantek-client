export class NvmNoisePlotOptions {

  public static refreshInterval: number = 60 * 1000;
  public static options: any = {
    responsive: true,
    scales: {
      xAxes: [{
        type: 'time',
        time: {
          displayFormats: {
            'millisecond': 'MMM DD h:mm:ss a',
            'second': 'MMM DD h:mm:ss a',
            'minute': 'MMM DD h:mm:ss a',
            'hour': 'MMM DD h:mm:ss a',
            'day': 'MMM DD h:mm:ss a',
            'week': 'MMM DD h:mm:ss a',
            'month': 'MMM DD h:mm:ss a',
            'quarter': 'MMM DD h:mm:ss a',
            'year': 'MMM DD h:mm:ss a',
          }
        }
      }],
    },
    legend: {
      position: 'bottom',
    },
    elements: {
      point: {
        radius: 0,
        hitRadius: 10,
        hoverRadius: 5,
      },
      line: {
        tension: 0,
        borderWidth: 1.5,
        fill: false
      },
    },
    pan: {
      enabled: true,
      mode: 'xy'
    },
    zoom: {
      enabled: true,
      mode: 'x',
    },
    tooltips: {
      callbacks: {
        label: function (tooltipItem, data) {

          var label = '';

          if (data.datasets[tooltipItem.datasetIndex].label && tooltipItem && tooltipItem.yLabel) {
            label = `${data.datasets[tooltipItem.datasetIndex].label}: ${tooltipItem.yLabel} dB`;
          }

          return label;
        }
      }
    }
  };
}