import { Component, OnInit, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

import { NvmNoisePlotService } from '../../services/nvm-noise-plot.service';
import { NvmNoisePlotOptions } from '../../nvm-noise-plot-options';
import { PrintCanvasService } from '../../../../../shared/services/print-canvas.service';
import { PrintClickService } from '../../../../../shared/services/print-click.service';

import { interval } from 'rxjs/observable/interval';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-nvm-noise-plot',
  templateUrl: './nvm-noise-plot.component.html',
  styleUrls: ['./nvm-noise-plot.component.scss']
})
export class NvmNoisePlotComponent implements OnInit, OnDestroy {

  @ViewChild('plot') plotRef: ElementRef;

  public lineChartLabels: Array<any> = [];
  public lineChartData: Array<any>;
  public lineChartOptions: any;
  public latestResults: any;
  private printClickedSubscription: Subscription;

  constructor(
    private nvmDataService: NvmNoisePlotService,
    private printCanvasService: PrintCanvasService,
    private printClickService: PrintClickService) {
  }

  ngOnInit() {
    this.lineChartOptions = NvmNoisePlotOptions.options;
    this.lineChartData = this.nvmDataService.getInitialData();
    this.getPlotData();

    const refreshInterval = interval(NvmNoisePlotOptions.refreshInterval);
    refreshInterval.subscribe(x => this.getPlotData());

    this.printClickedSubscription = this.printClickService.printClicked$
      .subscribe(
        () => this.printPlot()
      );
  }

  getPlotData() {

    const serialNumber = '45458';
    const startDate =  '2017-05-19';
    const endDate =  '2017-06-30';

    this.nvmDataService.getData(serialNumber, startDate, endDate)
      .subscribe(data => {
        let dto = this.nvmDataService.toNvmNoisePlotModel(data);
        this.lineChartLabels.length = 0;

        for (let i = 0; i < dto.plotLabels.length; i++) {
          this.lineChartLabels.push(dto.plotLabels[i]);
        }

        this.lineChartData = dto.plotData;
      });
  }

  printPlot() {
    this.printCanvasService.print(<HTMLCanvasElement>this.plotRef.nativeElement);
  }

  ngOnDestroy() {
    this.printClickedSubscription.unsubscribe();
  }
}
