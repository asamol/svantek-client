import { Component, OnInit } from '@angular/core';
import { NvmNoiseLatestResultsService } from '../../services/nvm-noise-latest-results.service';
import { NvmNoiseLatestResultsModel } from '../../models/nvm-noise-latest-results.model';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-nvm-noise-latest-results',
  templateUrl: './nvm-noise-latest-results.component.html',
  styleUrls: ['../../../nvm.scss']
})
export class NvmNoiseLatestResultsComponent implements OnInit {

  latestResults$: Observable<NvmNoiseLatestResultsModel>;

  constructor(public latestResultsService: NvmNoiseLatestResultsService) { }

  ngOnInit() {

    const serialNumber = '45458';
    this.latestResults$ = this.latestResultsService.getData(serialNumber);

  }
}
