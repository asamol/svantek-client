import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartsModule } from 'ng2-charts';

import { NvmNoisePlotService } from './services/nvm-noise-plot.service';
import { NvmNoiseLatestResultsComponent } from './components/nvm-noise-latest-results/nvm-noise-latest-results.component';
import { NvmNoisePlotComponent } from './components/nvm-noise-plot/nvm-noise-plot.component';
import { NvmNoiseLatestResultsService } from './services/nvm-noise-latest-results.service';
import { NvmNavigationComponent } from '../nvm-navigation/nvm-navigation.component';
import { NvmHomeComponent } from '../nvm-home/nvm-home.component';
import { NvmActionsComponent } from '../nvm-actions/nvm-actions.component';
import { SharedModule } from '../../../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    ChartsModule,
    SharedModule
  ],
  declarations: [
    NvmHomeComponent,
    NvmNoisePlotComponent,
    NvmNoiseLatestResultsComponent,
    NvmNavigationComponent,
    NvmActionsComponent
  ],
  exports: [NvmHomeComponent],
  providers: [
    NvmNoisePlotService,
    NvmNoiseLatestResultsService
  ]
})
export class NvmNoiseModule { }
