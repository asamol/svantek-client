export class NvmNoiseLatestResultsModel {
    public constructor(
        public dateTime : Date, 
        public laeq : Number,
        public lamax : Number,
        public l10 : Number,
        public l90 : Number,
        public laeqn : Number){ }
}