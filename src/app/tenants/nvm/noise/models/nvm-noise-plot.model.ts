import { NvmNoiseLatestResultsModel } from "./nvm-noise-latest-results.model";

export class NvmNoisePlotModel {
    public constructor(
        public plotLabels: Date[], 
        public plotData: { label: string, data: any[] }[]) { }
}