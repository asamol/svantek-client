import { Injectable } from '@angular/core';
import { HttpParams, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { NvmNoisePlotModel } from '../models/nvm-noise-plot.model';

@Injectable()
export class NvmNoisePlotService {

  constructor(private http: HttpClient) { }

  laeq: string = "LAeq";
  lamax: string = "LAmax";
  l10: string = "L10";
  l90: string = "L90";
  laeq1h: string = "LAeq 1h";

  public getInitialData() {
    return [
      { label: this.laeq, data: [] },
      { label: this.lamax, data: [] },
      { label: this.l10, data: [] },
      { label: this.l90, data: [] },
      { label: this.laeq1h, data: [] }
    ];
  }

  public getData(stationCode: string, startDate: string, endDate: string) {

    const params = new HttpParams()
      .set('stationCode', stationCode)
      .set('startDate', startDate)
      .set('endDate', endDate);

    const apiUrl = "https://localhost:44317/home/GetNvmNoiseData";

    return this.http.get(apiUrl, { params: params });
  }

  public toNvmNoisePlotModel(data: any): NvmNoisePlotModel {

    const plotData = [
      { label: "LAeq", data: data.map(x => x.laeq) },
      { label: "LAmax", data: data.map(x => x.lamax) },
      { label: "L10", data: data.map(x => x.l10) },
      { label: "L90", data: data.map(x => x.l90) },
      { label: "LAeq 1h", data: data.map(x => x.laeqn) }
    ];

    return new NvmNoisePlotModel(data.map(x => new Date(x.dateTime)), plotData);
  }
}


