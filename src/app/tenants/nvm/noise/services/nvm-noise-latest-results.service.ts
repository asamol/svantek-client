import { Injectable } from '@angular/core';
import { HttpParams, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { NvmNoiseLatestResultsModel } from '../models/nvm-noise-latest-results.model';

@Injectable()
export class NvmNoiseLatestResultsService {

    constructor(private http: HttpClient) { }

    public getData(stationCode: string) {
        
        const params = new HttpParams()
            .set('stationCode', stationCode);

        const apiUrl = "https://localhost:44317/home/GetNvmNoiseLatestResults";

        return this.http.get<NvmNoiseLatestResultsModel>(apiUrl, { params: params });
    }

    public toModel(data: any): NvmNoiseLatestResultsModel {

        return new NvmNoiseLatestResultsModel(
            new Date(data.dateTime),
            data.laeq,
            data.lamax,
            data.l10,
            data.l90,
            data.laeqn
        );
    }
}


