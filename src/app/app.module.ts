import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http'; 

import { AppComponent } from './app.component';
import 'chartjs-plugin-zoom';
import { NvmNoiseModule } from './tenants/nvm/noise/nvm-noise.module';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    NvmNoiseModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
