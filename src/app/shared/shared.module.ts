import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PrintCanvasService } from './services/print-canvas.service';
import { WindowRefService } from './services/window-ref.service';

@NgModule({
  imports: [
    CommonModule
  ],
  providers: [
    PrintCanvasService,
    WindowRefService
  ],
  declarations: []
})
export class SharedModule { }
