import { Injectable } from '@angular/core';
import { WindowRefService } from './window-ref.service';

@Injectable()
export class PrintCanvasService {

  constructor(private windowRefService: WindowRefService) { }

  public print(canvas: HTMLCanvasElement) {
    let dataUrl = canvas.toDataURL();
    let windowContent = this.getPrintLayout(dataUrl);

    var printWin = this.windowRefService.getNativeWindow().open('', '');
    printWin.document.open();
    printWin.document.write(windowContent);
    printWin.document.close();
    printWin.document.addEventListener('load', function () {
      printWin.focus();
      printWin.print();
      printWin.document.close();
      printWin.close();
    }, true);
  }

  private getPrintLayout(dataUrl: string) {
    return `<!DOCTYPE html>
    <html>
      <head>
        <title>Print chart</title>
      </head>
      <body>
        <img src='${dataUrl}'>
      </body>
    </html>`;
  }
}
