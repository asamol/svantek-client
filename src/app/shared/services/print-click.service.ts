import { Injectable } from '@angular/core';
import { Subject }    from 'rxjs';

@Injectable()
export class PrintClickService {

  private printClickedSource = new Subject();
  printClicked$ = this.printClickedSource.asObservable();

  printClick(){
    this.printClickedSource.next();
  }
}
